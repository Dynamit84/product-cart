$(function () {
    var shop = new application.collections.ProductsCollection();

    new application.views.ProductsCollectionView({ collection: shop });
});