(function (app) {
    app.views.ProductView = Backbone.View.extend({
        className:'.product',
        cart: null,
        template: app.templates.getTemplateByID('product-template'),
        events: {
            'click .buy': 'addToCart'
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));

            return this;
        },
        addToCart: function () {
            this.cart = this.getCart();
            var cartView = new app.views.CartView({collection: this.cart});
            cartView.collection.add(this.model);
            Backbone.trigger('renderCart', cartView);
        },
        getCart: function () {
            function createCart () {

                return new application.collections.Cart();
            }

            return (function () {
                if(!this.cart) {
                    this.cart = createCart();
                }

                return this.cart;
            })();
        }
    });
})(application);