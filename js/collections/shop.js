(function (app) {
    app.collections.ProductsCollection = Backbone.Collection.extend({
        model: app.models.ProductModel,
        url: 'json/data.json',
        initialize: function () {
            this.fetch({reset: true});
        }
    });
})(application);